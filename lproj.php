#!/usr/bin/env php
<?php

function fmt($str){
  return addcslashes($str,"\0..\37");
}

$paths=array();
exec('find '.escapeshellarg($_SERVER['argv'][1]?:'.').
 ' -path */English.lproj/*.strings -or -path */en.lproj/*.strings',$paths);

$lproj=array();
foreach($paths as $fn){
  $plist=json_decode(shell_exec('plutil -convert json -o - '.escapeshellarg($fn)));
  if(!$plist){continue;}
  foreach($plist as $key=>$str){
    if(is_string($str)){$lproj[]=fmt($str)."\t".fmt($key)."\t$fn\n";}
  }
}

natcasesort($lproj);
foreach($lproj as $line){echo $line;}

?>
